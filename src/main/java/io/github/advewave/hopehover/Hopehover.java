package io.github.advewave.hopehover;

import org.bukkit.plugin.java.JavaPlugin;

public final class Hopehover extends JavaPlugin {

    @Override
    public void onEnable() {
        // Plugin startup logic
        System.out.println("[HopeHover] HopeHover ${project.version} has started up!");
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        System.out.println("[HopeHover] HopeHove ${project.version}r is shutting down.");
    }
}
